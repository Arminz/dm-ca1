Data Set Information:

Attribute Information:

InvoiceNo: Invoice number. a 6-digit integral number uniquely assigned to each transaction. If this code starts with letter 'c', it indicates a cancellation. 
StockCode: Product (item) code. a 5-digit integral number uniquely assigned to each distinct product. 
Description: Product (item) name. 
Quantity: The quantities of each product (item) per transaction. 	
InvoiceDate: Invice Date and time. the day and time when each transaction was generated. 
UnitPrice: Unit price. Product price per unit in sterling. 
CustomerID: Customer number.  a 5-digit integral number uniquely assigned to each customer. 
Country: Country name. the name of the country where each customer resides.

