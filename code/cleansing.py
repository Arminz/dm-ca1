
# coding: utf-8

# In[46]:


import pandas as pd
import numpy as np
from scipy import stats


# In[89]:


# age, workclass, fnlwgt, education, education-num, marital-status, occupation, relationship, race, sex, capital-gain, capital-loss, hours-per-week, native-country, Label
income = pd.read_csv('./data/cleansing/income.data', delimiter=', ')


# In[49]:


income.head(3)


# In[51]:


income.describe(include='all')


# In[52]:


a = (income['hours-per-week']).mean()
a


# In[62]:


for column in income.columns:
        income[column] = np.where(income[column].astype('str') == '?', stats.mode(income[column])[0][0], income[column])
income.head(2)


# In[82]:


for column in income.columns:
    if income[column].dtype == np.dtype('float64'):
        income[column] = np.where(income[column].isna(), income[column].mean(), income[column])
income.head(2)


# In[83]:


income[income['hours-per-week'] == a].head(4)


# In[84]:


income[income['hours-per-week'] < 0]


# In[85]:


income[income['age'] < 16]


# In[86]:


avg = income['age'].mean()
mask = income['age'] < 16
income.loc[mask, 'age'] = avg


# In[87]:


income.to_csv('./result/dataCleansing.csv', index=False)

