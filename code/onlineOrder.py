
# coding: utf-8

# <h1><b>
# Armin Zirak - 810194500 - Data Mining CA1 Report
# </h1>

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sb


# In[2]:


orders = pd.read_excel('./data/onlineOrder/onlineOrder.xlsx')
orders.head(3)


# In[3]:


orders.count()


# <h1>Part 1</h1>

# In[4]:


orders.describe(include='all')


# In[5]:


orders.dtypes


# In[6]:


[orders['InvoiceDate'].min(), orders['InvoiceDate'].max()]


# <h3> Result Note </h3>

# InvoiceNo -> Categorical <br>
# StockCode -> Categorical <br>
# Description -> Categorical </br>
# Quantity -> Numerical Min: -80995, Max: 80995 <br>
# InvoiceDate -> Numerical Min: '2010-12-01 08:26:00', Max: '2011-12-09 12:50:00' <br>
# UntiPrice -> Numerical Min: -11062, Max: 38970 <br>
# CustomerId -> Categorical Min: 12346, Max: 18278 <br>
# Country -> Categorical <br>
# 
# 

# <h2> Part 2 </h2>

# In[8]:


orders['year'] = orders['InvoiceDate'].map(lambda x: x.year)
orders['day'] = orders['InvoiceDate'].map(lambda x: x.day)
orders['month'] = orders['InvoiceDate'].map(lambda x: x.month)
orders['hour'] = orders['InvoiceDate'].map(lambda x: x.hour)
orders.head(2)


# <h3>Part 3</h3>

# In[9]:


orders['totalPrice'] = orders['Quantity'] * orders['UnitPrice']
orders.head(2)


# <h1> Part 4 </h1>

# In[10]:


x = orders['hour']
plt.hist(x)
plt.axvline(x.mean(), color='black', linestyle='dashed', linewidth=2, label='mean')
plt.axvline(x.median(), color='r', linestyle='dashed', linewidth=2, label='median')
plt.axvline(stats.mode(x)[0][0], color='y', linestyle='dashed', linewidth=2, label='mode')
plt.legend()
plt.xlabel('hour')
plt.ylabel('count')
plt.title('Hour Histogram')


# In[11]:


mean = x.mean()
median = x.median()
mode = stats.mode(x)[0][0]
skew = stats.skew(x)
[mean, median, mode, skew]


# <h3>Result Note</h3>
# mean -> black line (13.07) <br>
# median -> red line (13.0) <br>
# mode -> yellow line (12) <br>
# skewness -> positive (0.005) <br>
# <p dir='rtl'>
# مدین بین مد و میانگین است و چونگی مثبت می باشد.
# </p>
# 

# <h1> Part 5</h1>

# <h3> Pie Chart </h3>

# In[12]:


orders['Country'].value_counts().plot(kind='pie').set_title('Pie Chart')
plt.legend(loc='lower right', bbox_to_anchor=(3,0), ncol=3)


# <h3> Bar Plot </h3>

# In[13]:


orders['Country'].value_counts().plot(kind='bar').set_title('Bar Plot')


# <h3> Result Note </h3>
# کشورها عمدتا بریتانیا هستند و از مابقی کشورها هر کدام تعداد کمی وجود دارد

# <h1> Part 6 </h1>

# In[14]:


green_diamond = dict(markerfacecolor='r', marker='s')
plt.boxplot(orders['day'], flierprops=green_diamond, vert=False, whis=1.5)
plt.xlabel('day')
plt.title('Day BoxPlot')
# orders['Quantity'].describe()


# In[16]:


green_diamond = dict(markerfacecolor='r', marker='s')
plt.boxplot(orders['totalPrice'], flierprops=green_diamond, vert=False, whis=1.5)
plt.title('totalPrice BoxPlot')
plt.xlabel('totalPrice')


# <h3> Result Note </h3>
# <p dir='rtl'>
# داده پرتی در این ویژگی اول وجود نداشته و تمام داده ها داخل بازه ۱.۵ برابر هستند.
# اما در ویژگی دوم برعکس هست و به دلیل چگالی زیاد داده ها در یک بازه بسیار کوتاه و وجود داده های خیلی پرت در فاصله زیاد عملا باکس اصلی دیده نمی شود. نقاط قرمز همه داده پرت هستند که در فاصله دور از باکس وجود دارند.
# </p>
# 

# <h1> Part 7 </h1>

# In[18]:


x = orders['Quantity']
y = orders['totalPrice']
plt.scatter(x, y)
np.corrcoef(x, y)
plt.xlabel('totalPrice')
plt.ylabel('Quantity')
plt.title('TotalPrice / Quantity Scatter Plot')


# <h3> Result Note </h3>
# <p dir='rtl'>
# تنها ویژگی هایی که با هم ارتباط کافی داشتند مقدار و قیمت نهایی بودند (که بر اساس هم ساخته شده اند) ارتباط آن ها در نمودار مشخص است و ضریب کوریلیشاشن نیز ۰.۸۸۶۶ است.
# </p>
# 

# <h1> Part 8 </h1>

# In[19]:


h = orders.groupby('hour').sum()['Quantity']
# plt.scatter(h)


# In[20]:


h.plot()
plt.title('Quantity Per Hour Plot')
plt.ylabel('Quantity')
plt.xlabel('hour')


# In[21]:


orders.Country.unique()


# In[22]:


for country in orders.Country.unique()[:2]:
    country_order = orders[orders.Country == country]
    all_h = country_order['Quantity'].sum()
    h = country_order.groupby('hour')['Quantity'].sum()
    (h/all_h).plot()
plt.legend(orders.Country.unique(), bbox_to_anchor=(1,1), loc='upper right', ncol=1)
plt.title('Quantity Per Hour Plot')
plt.ylabel('Quantity')
plt.xlabel('hour')


# <h3> Result Note </h3>
# <p dir='rtl'>
# روابط مختلف را با هم بررسی کردم با توجه به قناس بودن داده ها بهترین نتایجی که گرفتم موارد بالا بودند.
# در اولی نمودار فروش در ساعات مختلف روز بررسی شده که در میانه آن بیشترین است و در دومی میزان فروش در ساعات مختلف کشورهای متفاوت با هم بررسی شده است.
# </p>
